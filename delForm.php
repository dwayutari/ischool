<?php
// session_start();
if(isset($_SESSION['idStudentDel'])){
	$id = $_SESSION['idStudentDel'];
	// $nama = $_POST['nama'];
	// $nim = $_POST['nim'];
	// $nilai = $_POST['nilai'];
	// $tanggal = $_POST['tanggal'];
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.apigo.id/2/classes/Student/" . $id,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "DELETE",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache",
	    "content-type: application/json",
	    // "postman-token: a46e3620-1e34-a421-3a4a-7c4bcb53e569",
	    "x-mesosfer-application-id: RYDml80",
	    "x-mesosfer-client-key: ceCLZaz8BeTzdFq78f6B1WPimAy0BH35"
	  ),
	));


	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  // echo $response;
	}

	// header("Location: /index.php?page=student");
	// exit();
}
?>

<script> location.replace("index.php?page=student"); </script>