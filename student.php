<?php
	// $url = "https://api.apigo.id/2/users";
	// $fields = array("username" => "xhely.jackie@gmail.com", "firstname" => "dewa");

	// $result = sendTrigger($url, $fields);

	// foreach( $result as $test => $array){
	// 	echo $test . " ";
	// }

	// function sendTrigger($url, $fields){  
	//     // $fields = json_encode($fields);
	//     $ch = curl_init();
	//     curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=UTF-8"));
	//     curl_setopt($ch, CURLOPT_URL, $url);
	//     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	//     // curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	//     $curlResult["msg"] = curl_exec($ch);
	//     $curlResult["http"] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	//     curl_close($ch);
	//     return $curlResult;
	// }
?>

<div class="container-fluid container-fullw">
	<div class="row">
		<div class="col-md-12">
			<h5 class="over-title margin-bottom-15">Master <span class="text-bold">Student</span></h5>
			<div class="row">
				<div class="col-md-12 space20">
					<a class="btn btn-green add-row" href="index.php?page=stuNew">
						Add New <i class="fa fa-plus"></i>
					</a>
				</div>
			</div>
			<div class="table-responsive">
				<table class="table table-striped table-hover" id="sample_2">
					<thead>
						<tr>
							<th>Nama</th>
							<th>NIM</th>
							<th>Nilai</th>
							<th>Tanggal</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
					<?php

						$curl = curl_init();

						curl_setopt_array($curl, array(
						  CURLOPT_URL => "https://api.apigo.id/2/classes/Student",
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "GET",
						  CURLOPT_HTTPHEADER => array(
						    "cache-control: no-cache",
						    // "postman-token: 1a968313-9a07-f3b6-0608-e06cf87170d7",
						    "x-mesosfer-application-id: RYDml80",
						    "x-mesosfer-client-key: ceCLZaz8BeTzdFq78f6B1WPimAy0BH35"
						  ),
						));

						$response = curl_exec($curl);
						$err = curl_error($curl);

						curl_close($curl);

						if ($err) {
						  echo "cURL Error #:" . $err;
						} else {
						  $data = json_decode($response, true);
						}
					?>
					<?php foreach ($data["results"] as $row) : ?>
						<tr>
							
							<td><?php echo $row["nama"]; ?></td>
							<td><?php echo $row["nim"]; ?></td>
							<td><?php echo $row["nilai"]; ?></td>
							<td><?php echo isset($row["tanggal"]) ? date('d-m-Y', strtotime($row["tanggal"]["iso"])) : '-'; ?></td>
							<td>
								<a class="btn btn-blue add-row" href="index.php?page=stuEdit&id=<?php echo $row["objectId"]; ?>">Edit</a>
							</td>
							<td>
								<a class="btn btn-red add-row" href="index.php?page=stuDelete&id=<?php echo $row["objectId"]; ?>">Delete</a>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>