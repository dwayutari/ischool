<?php
$json = file_get_contents("php://input");
$obj = json_decode($json);
$response = array();
$response["nama"] = $obj->nama;
$response["nim"] = $obj->nim;
$response["nilai"] = $obj->nilai;
$response["tanggal"] = $obj->tanggal;
$json_response = json_encode($response);
echo $json_response;
header("Content-type: application/json");
header("location:index.php?page=student");
?>