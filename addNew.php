<?php
if(isset($_POST['add'])){
	$nama = $_POST['nama'];
	$nim = $_POST['nim'];
	$nilai = $_POST['nilai'];
	$tanggal = str_replace('+00:00', '.000Z', gmdate('c', strtotime($_POST['tanggal'])));
	// echo "tgl : ".$tanggal;
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.apigo.id/2/classes/Student",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => "{\"nama\":\"$nama\",\"nim\":\"$nim\",\"nilai\":$nilai, \"tanggal\":{\"__type\": \"Date\",\"iso\": \"$tanggal\"}}",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache",
	    // "postman-token: 7fd307c5-4301-8e11-a8ce-4a7a7d892c68",
	    "x-mesosfer-application-id: RYDml80",
	    "x-mesosfer-client-key: ceCLZaz8BeTzdFq78f6B1WPimAy0BH35"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  echo $response;
	}

	header("location:index.php?page=student");
}
?>