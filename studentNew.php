

<div class="container-fluid container-fullw">
	<div class="row">
		<div class="col-md-12">
			<h5 class="over-title margin-bottom-15">Master <span class="text-bold">Student</span></h5>
			<div class="table-responsive">
				<div class="panel panel-transparent">
					<div class="panel-heading">
						<h5 class="panel-title">Create New Master Student</h5>
					</div>
					<div class="panel-body">
						<form role="form" class="form-horizontal" method="POST" action="addNew.php">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="nama">
									Nama
								</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Nama Mahasiswa" id="nama" name="nama" class="form-control underline">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="nim">
									NIM
								</label>
								<div class="col-sm-10">
									<input type="text" placeholder="NIM" id="nim" name="nim" class="form-control underline">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="nilai">
									Nilai
								</label>
								<div class="col-sm-10">
									<input type="number" placeholder="nilai" id="nilai" name="nilai" class="form-control underline" min="0" max="100">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="tanggal">
									Tanggal
								</label>
								<div class="col-sm-10">
									<input type="date" placeholder="Tanggal" id="tanggal" name="tanggal" class="form-control underline">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary pull-right" name="add">
										Submit <i class="fa fa-arrow-circle-right"></i>
									</button>
								<!-- </div> -->
								<!-- <div class="col-sm-6"> -->
									<a href="index.php?page=student" class="btn btn-danger pull-right">
										Cancel </i>
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>