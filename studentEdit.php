<?php
// echo "id : ". $_SESSION['idStudent'];
$stuArray = array();
$nama = '';
$nim = '';
$nilai = '';
$tanggal = '';
if(isset($_SESSION['idStudent'])){
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.apigo.id/2/classes/Student/" . $_SESSION['idStudent'],
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "cache-control: no-cache",
	    // "postman-token: ecaed40b-642c-5dd4-75e3-649bf41f0b67",
	    "x-mesosfer-application-id: RYDml80",
	    "x-mesosfer-client-key: ceCLZaz8BeTzdFq78f6B1WPimAy0BH35"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	$stuArray = $response;
	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
		$data = json_decode($response);
		// $data1 = json_decode($data->tanggal);
	}

	
}
?>

<div class="container-fluid container-fullw">
	<div class="row">
		<div class="col-md-12">
			<h5 class="over-title margin-bottom-15">Master <span class="text-bold">Student</span></h5>
			<div class="table-responsive">
				<div class="panel panel-transparent">
					<div class="panel-heading">
						<h5 class="panel-title">Edit Master Student</h5>
					</div>
					<div class="panel-body">
						<form role="form" class="form-horizontal" method="POST" action="editForm.php">
							<!-- <input type="hidden" id="idStudent" name="idStudent" value="<?php echo $data->objectID; ?>"> -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="nama">
									Nama
								</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Nama Mahasiswa" id="nama" name="nama" class="form-control underline" value="<?php echo $data->nama; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="nim">
									NIM
								</label>
								<div class="col-sm-10">
									<input type="text" placeholder="NIM" id="nim" name="nim" class="form-control underline" value="<?php echo $data->nim; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="nilai">
									Nilai
								</label>
								<div class="col-sm-10">
									<input type="number" placeholder="nilai" id="nilai" name="nilai" class="form-control underline" min="0" max="100" value="<?php echo $data->nilai; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="tanggal">
									Tanggal
								</label>
								<div class="col-sm-10">
									<input type="date" placeholder="Tanggal" id="tanggal" name="tanggal" class="form-control underline" value="<?php print_r(date('Y-m-d', strtotime($data->tanggal->iso))); ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary pull-right" name="edit">
										Submit <i class="fa fa-arrow-circle-right"></i>
									</button>
								<!-- </div> -->
								<!-- <div class="col-sm-6"> -->
									<a href="index.php?page=student" class="btn btn-danger pull-right">
										Cancel </i>
									</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>